# hackathon-front-end
## Installation
```
npm i
npm start
```

## Info
Front-end was written on **React.js**

Back-end was written on **Node-Red** + **MongoDB**. 

[Back-end Editor Link](https://hackathon-int20h.herokuapp.com) 

Login/password will be given by request.
