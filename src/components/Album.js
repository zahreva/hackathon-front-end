import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import Dialog from '@material-ui/core/Dialog';
import classNames from 'classnames';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import Slide from '@material-ui/core/Slide';
import Pagination from 'material-ui-flat-pagination';
import DialogContent from '@material-ui/core/DialogContent';

import MenuListComposition from './MenuListComposition';

import styles from '../utils/theme';
import AlertDialog from './AlertDialog';

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}


class Album extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emotion: 'Pick an emotion',
      page: 0,
      offset: 0,
      photos: [],
      open: false,
      currentImage: '',
      currentEmotions: [],
      lastUpdatedAt: 'long time ago',
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleEmotion = this.handleEmotion.bind(this);
  }

  componentDidMount() {
    this.loadPhotos();
    axios.get('https://hackathon-int20h.herokuapp.com/lastUpdateCash')
      .then((response) => {
        const { lastUpdatedAt } = response.data;
        this.setState(state => ({
          ...state,
          lastUpdatedAt,
        }));
      })
      .catch(err => console.error(err));

  }

  handleEmotion = (emotion) => {
    this.setState({ emotion, offset: 0, page: 0 }, () => {
      this.loadPhotos();
    });
  };

  handleClick = (offset, page) => {
    this.setState({ offset, page: page - 1 }, () => {
      this.loadPhotos();
    });
  };

  handleClickOpen = (photo) => {
    const { url, emotions } = photo;
    this.setState({ open: true, currentImage: url, currentEmotions: emotions });
  };

  handleClose = () => {
    this.setState({ open: false, currentImage: null });
  };

  loadPhotos = () => {
    const { page } = this.state;
    let { emotion } = this.state;
    emotion = emotion === 'Pick an emotion' ? 'all' : emotion.toLowerCase();
    axios.get('https://hackathon-int20h.herokuapp.com/getPhotos', {
      params: { emotion, page },
    })
      .then((response) => {
        const { limit, total, photos } = response.data;
        this.setState(state => ({
          ...state,
          limit,
          total,
          photos,
        }));
      });
  };

  calculateFaces = (emotions) => {
    let faces = 0;

    for (const emotion of emotions) {
      faces += emotion.faces;
    }
    return faces;
  };

  render() {
    const { classes } = this.props;
    const {
      emotion, offset, total, photos, open, currentImage, currentEmotions, lastUpdatedAt,
    } = this.state;

    return (
      <React.Fragment>
        <CssBaseline />
        <main>
          {/* Hero unit */}
          <div className={classes.heroUnit}>
            <div className={classes.heroContent}>
              <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Emotion picker by PRIME
              </Typography>
              {/* <Typography variant="h4" align="center" color="textSecondary" paragraph> */}
              {/* Simple App that shows you images with emotions that you have chosen. */}
              {/* </Typography> */}
              <Typography variant="h4" align="center" color="textSecondary" paragraph>
                Click on the photo to see details about face emotion analysis.
              </Typography>
              <div className={classes.heroButtons}>
                <Grid container spacing={16} justify="center">
                  <Grid item>
                    <MenuListComposition
                      currentEmotion={emotion}
                      handleEmotion={this.handleEmotion}
                    />
                  </Grid>
                  <Grid item>
                    <AlertDialog lastUpdatedAt={lastUpdatedAt} />
                  </Grid>
                </Grid>
              </div>
            </div>
          </div>
          <div className={classNames(classes.layout, classes.cardGrid)}>
            {/* End hero unit */}
            <Grid container spacing={40}>
              {photos.map(photo => (
                <Grid item key={photo._id} sm={6} md={4} lg={3}>
                  <Card className={classes.card}>
                    <CardMedia
                      style={{ cursor: 'pointer' }}
                      onClick={() => this.handleClickOpen(photo)}
                      className={classes.cardMedia}
                      image={photo.url}
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h5">
                        Detected faces:
                        <span>{this.calculateFaces(photo.emotions)}</span>
                      </Typography>
                      <Typography variant="h5">
                        {photo.date}
                      </Typography>
                    </CardContent>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </div>
        </main>
        <div className="pagination">
          <CssBaseline />
          <Pagination
            limit={12}
            offset={offset}
            total={total}
            onClick={(e, offset, pageNumber) => this.handleClick(offset, pageNumber)}
            size="large"
          />
        </div>
        {/* Footer */}
        <footer className={classes.footer} style={{ padding: '1rem', marginTop: '2rem' }}>
          <Typography variant="h5" align="center" gutterBottom>
            Links to our LinkedIn
          </Typography>
          <Typography align="center" color="textSecondary" component="p" variant="h4" style={{ marginBottom: '1rem', marginTop: '1rem' }}>
            <Link href="https://www.linkedin.com/in/nazariibaran/" className={classes.link}>
              Nazar Baran
            </Link>
            <Link href="https://www.linkedin.com/in/yuriy-zahreva-407710166/" className={classes.link}>
              Yuriy Zahreva
            </Link>
            <Link href="https://www.linkedin.com/in/marta-bobyk-349288163/" className={classes.link}>
              Marta Bobyk
            </Link>
          </Typography>
        </footer>
        {/* End footer */}
        <Dialog
          // fullScreen
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          keepMounted
        >
          <img
            alt="fullImg"
            style={{ cursor: 'pointer' }}
            onClick={this.handleClose}
            height="100%"
            width="100%"
            src={currentImage}
          />
          <DialogContent style={{ padding: '2rem' }}>
            <div align="center" style={{ fontSize: 15 }}>
              {currentEmotions.map((currentEmotion) => {
                const { isAverage, percentage, faces } = currentEmotion;
                if (isAverage) {
                  return (
                    <div key={percentage}>
                      <span>
                        {`Emotion
                        "${capitalizeFirstLetter(currentEmotion.emotion)}"
                        has average percentage around ${percentage.toFixed(2)}%
                        among ${faces} people.` }
                      </span>
                    </div>
                  );
                }
                return (
                  <div key={percentage}>
                      <span>
                        {`Emotion
                        "${capitalizeFirstLetter(currentEmotion.emotion)}"
                        percentage around ${percentage.toFixed(2)}%
                        ` }
                      </span>
                  </div>
                );
              })
              }
            </div>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}

Album.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withStyles(styles)(Album);
