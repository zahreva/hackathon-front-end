import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import { ClipLoader } from 'react-spinners';
import { css } from '@emotion/core';

const override = css`
    display: block;
    margin: 0 auto;
`;

class AlertDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      isLoading: false,
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleUpdate = async () => {
    this.setState(state => ({
      ...state,
      isLoading: true,
    }));
    await axios.post('https://hackathon-int20h.herokuapp.com/updateCash')
      .then(() => {
        window.location.reload();
      })
      .catch(err => console.error(err));
  };

  render() {
    const { open, isLoading } = this.state;
    const { lastUpdatedAt } = this.props;
    return (
      <div>
        <Button variant="outlined" color="primary" onClick={this.handleClickOpen} style={{ fontSize: '1.4rem' }}>
          Refresh Cache
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title" style={{ paddingTop: '1.5rem', paddingBottom: '.5rem', textAlign: 'center' }}>
            <Typography variant="h3">Are you sure?</Typography>
          </DialogTitle>
          <DialogContent style={{ padding: '2rem' }}>
            <DialogContentText id="alert-dialog-description" style={{ fontSize: '1.5rem', textAlign: 'center' }}>
              Due Cognitive++ API`s free tier limitation, update can take up to a minute.
              Please, don’t leave page until response.
            </DialogContentText>
            <DialogContentText id="alert-dialog-description" style={{ fontSize: '1.5rem', textAlign: 'center', padding: '2rem' }}>
              Last time updated at:
              {` ${lastUpdatedAt}`}
            </DialogContentText>
          </DialogContent>
          <ClipLoader
            css={override}
            sizeUnit="rem"
            size={5}
            color="#123abc"
            loading={isLoading}
          />
          <DialogActions style={{ justifyContent: 'space-around', marginTop: '2rem' }}>
            <Button onClick={this.handleClose} color="secondary" variant="outlined" style={{ fontSize: '1.5rem' }}>
              No
            </Button>
            <Button onClick={() => this.handleUpdate()} color="primary" autoFocus variant="outlined" style={{ fontSize: '1.5rem' }}>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AlertDialog;
