import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  root: {
    display: 'flex',
  },
});

const emotions = ['All', 'Happiness', 'Surprise', 'Neutral', 'Sadness', 'Fear', 'Anger', 'Disgust'];

class MenuListComposition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    const { handleEmotion } = this.props;

    if (event.currentTarget.textContent) {
      handleEmotion(event.currentTarget.textContent);
    }

    this.setState({ open: false });
  };

  render() {
    const { classes, currentEmotion } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <div>
          <Button
            buttonRef={(node) => {
              this.anchorEl = node;
            }}
            aria-owns={open ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={this.handleToggle}
            style={{fontSize: '1.4rem'}}
          >
            {currentEmotion}
          </Button>
          <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList>
                      {emotions.map(emotion => (
                        <MenuItem
                          key={emotion}
                          style={{ fontSize: 16, padding: '1rem 2rem' }}
                          onClick={this.handleClose}
                        >
                          {emotion}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </div>
      </div>
    );
  }
}

MenuListComposition.propTypes = {
  classes: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  handleEmotion: PropTypes.func.isRequired,
  currentEmotion: PropTypes.string.isRequired,
};

export default withStyles(styles)(MenuListComposition);
