import React from 'react';
import ReactDOM from 'react-dom';

import './sass/main.scss';
import Album from './components/Album';

const App = () => (
  <div className="main">
    <Album />
  </div>
);

ReactDOM.render(<App />, document.querySelector('#root'));
